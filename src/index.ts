import { generateDependencyReport } from '@discordjs/voice';
import 'module-alias/register';
import DiscordBot from './bot';
import GatzeStriechelnCommand from './commands/funny/gatzeStreichelnCommand';
import ProstCommand from './commands/funny/prostCommand';
import StuhlCommand from './commands/funny/stuhlCommand';
import UltimatePettingCommand from './commands/funny/ultimatePettingCommand';
import YeetCommand from './commands/funny/yeetCommand';
import HelpCommand from './commands/utils/helpCommand';
import InviteCommand from './commands/utils/inviteCommand';
import ClearQueueCommand from './commands/voice/clearQueueCommand';
import JoinChannelCommand from './commands/voice/joinChannelCommand';
import LeaveChannelCommand from './commands/voice/leaveChannelCommand';
import LoopCommand from './commands/voice/loopCommand';
import NowPlayingCommand from './commands/voice/nowPlayingCommand';
import PauseCommand from './commands/voice/pauseCommand';
import PlayCommand from './commands/voice/playCommand';
import DisplayQueueCommand from './commands/voice/queueCommand';
import RemoveTrackCommand from './commands/voice/removeTrackCommand';
import SkipCommand from './commands/voice/skipCommand';
import StopCommand from './commands/voice/stopCommand';
import { prefix, token } from './config.json';

console.log(generateDependencyReport());

const bot = new DiscordBot(token, prefix);
bot.addCommand(new JoinChannelCommand(bot.musicHandler));
bot.addCommand(new LeaveChannelCommand(bot.musicHandler));
bot.addCommand(new PlayCommand(bot.musicHandler));
bot.addCommand(new NowPlayingCommand(bot.musicHandler));
bot.addCommand(new SkipCommand(bot.musicHandler));
bot.addCommand(new DisplayQueueCommand(bot.musicHandler, bot.client));
bot.addCommand(new ClearQueueCommand(bot.musicHandler));
bot.addCommand(new RemoveTrackCommand(bot.musicHandler));
bot.addCommand(new LoopCommand(bot.musicHandler));
bot.addCommand(new PauseCommand(bot.musicHandler));
bot.addCommand(new StopCommand(bot.musicHandler));

bot.addCommand(new GatzeStriechelnCommand());
bot.addCommand(new UltimatePettingCommand());
bot.addCommand(new StuhlCommand());
bot.addCommand(new ProstCommand());
bot.addCommand(new YeetCommand());

bot.addCommand(new HelpCommand(bot));
bot.addCommand(new InviteCommand());
bot.login();
