import DiscordBot from '@/bot';
import { DiscordGatewayAdapterCreator, entersState, getVoiceConnection, joinVoiceChannel, VoiceConnectionStatus } from '@discordjs/voice';
import { Snowflake } from 'discord-api-types';
import { Guild, StageChannel, VoiceChannel } from 'discord.js';
import Queue from './queue';

type AudioChannel = VoiceChannel | StageChannel;

/**
 * The configuration for the bot
 */
type MusicConfig = {
  /**
   * If the bot should leave on empty channel
   */
  leaveOnEmpty: boolean;
  /**
   * The amount of time waiting before leaving in seconds
   */
  waitBeforeLeave: number;
};

const DefaultMusicConfig: MusicConfig = {
  leaveOnEmpty: true,
  waitBeforeLeave: 30,
};

class MusicHandler /*extends TypedEmitter<MusicHandlerEvents>*/ {
  private readonly queues: Map<Snowflake, Queue> = new Map();

  private settings = DefaultMusicConfig;

  constructor(
    private readonly bot: DiscordBot,
  ) {
    if (this.settings.leaveOnEmpty) {
      bot.client.on('voiceStateUpdate', (oldState, newState) => {
        // if nobody left the channel in question, return.
        if (oldState.channelId !== oldState.guild?.me?.voice.channelId || newState.channel) { return; }

        if (oldState.channel !== null) {
          if (oldState.channel.members.size - 1 === 0) {
            setTimeout(() => {
              if (oldState.channel) {
                if (oldState.channel.members.size - 1 === 0) {
                  this.leave(oldState.channel);
                }
              }
            }, this.settings.waitBeforeLeave * 1000);
          }
        }
      });
    }
  }

  /**
   * Retrieves a queue from a given guild
   */
  public getQueue(guild: Guild): Queue {
    let queue = this.queues.get(guild.id);
    if (queue === undefined) {
      queue = new Queue(guild);
      this.queues.set(guild.id, queue);
    }
    return queue;
  }

  /**
   * Joins a given channel
   */
  public join(channel: AudioChannel, onReadyCb?: () => void): void {
    const guild = channel.guild;
    const queue = this.getQueue(guild);

    const connection = joinVoiceChannel({
      channelId: channel.id,
      guildId: guild.id,
      adapterCreator: guild.voiceAdapterCreator as DiscordGatewayAdapterCreator,
      selfMute: false,
    });

    connection.on(VoiceConnectionStatus.Ready, () => {
      queue.setReady(connection);

      if (onReadyCb !== undefined) {
        onReadyCb();
      }
    });

    connection.on(VoiceConnectionStatus.Disconnected, async () => {
      try {
        await Promise.race([
          entersState(connection, VoiceConnectionStatus.Signalling, 5_000),
          entersState(connection, VoiceConnectionStatus.Connecting, 5_000),
        ]);
        // Seems to be reconnecting to a new channel - ignore disconnect
      } catch (error) {
        // Seems to be a real disconnect which SHOULDN'T be recovered from
        this.leave(channel);
      }
    });
  }

  /**
   * Leaves a channel
   */
  public leave(channel: AudioChannel): void {
    const connection = getVoiceConnection(channel.guild.id);

    if (connection !== undefined) {
      try {
        connection.destroy();
      } catch (e) {
        console.error('Could not destroy connection', e);
      }
      const queue = this.getQueue(channel.guild);
      queue.clearQueue();
      queue.stopTrack();
    }
  }
}

export default MusicHandler;
