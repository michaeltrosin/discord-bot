import { Message } from 'discord.js';

enum ParameterType {
  String,
  Number,
  Mention,
}

enum CommandCategory {
  Voice,
  Utils,
  Funny,
}

type Parameter = {
  name: string;
  description: string;
  required: boolean;
};

type Parameters = Array<string>;

interface Command {
  /**
   * The name of the command
   */
  name: string;
  /**
   * The description of the command
   */
  description: string;
  /**
   * The aliases of the command
   */
  aliases: string[];
  /**
   * The parameters if any for the command
   */
  parameters?: Parameter[];
  /**
   * The category of this command
   */
  category: CommandCategory;
  /**
   * Will be called whenever the command gets eexcuted
   * @param msg The raw message
   * @param argc The argument count
   * @param argv The argument values
   */
  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void>;
}

export default Command;
export { Parameter as CommandParameter, Parameters, ParameterType, CommandCategory };
