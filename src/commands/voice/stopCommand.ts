import Command, { CommandCategory, CommandParameter, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';

class StopCommand implements Command {
  constructor(private handler: MusicHandler) { }

  name = 'stop';

  description = `Dieser Befehl beendet den spielenden Song`;

  aliases: string[] = [];

  parameters: CommandParameter[] = [];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }
    const guild = msg.guild;
    const queue = this.handler.getQueue(guild);

    queue.stopTrack();
  }
}

export default StopCommand;
