import Command, { CommandCategory, CommandParameter } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { LoopMode } from '@/music/queue';
import { Message } from 'discord.js';
import { prefix } from '@/config.json';

class LoopCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'loop';

  description = `Dieser befehl stellt den Loop modus ein. Genauere infos mit ${prefix}help loop.`;

  aliases = [];

  parameters: CommandParameter[] = [{
    name: 'type',
    description: 'Erlaubte Looptypen sind = Queue, Song, Disabled',
    required: false,
  }];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }

    const guild = msg.guild;
    const queue = this.musicHandler.getQueue(guild);

    const keys = Object.keys(LoopMode);

    if (parameters.length === 0) {
      let current: number = queue.currentLoopMode;
      current++;
      if (current >= keys.length / 2) {
        current = 0;
      }
      queue.setLoopMode(current);
    } else {
      const requestedLoop = parameters[0];
      queue.setLoopModeFromString(requestedLoop);
    }

    msg.reply(`Der ausgewählte loop modus: ${Object.keys(LoopMode)[queue.currentLoopMode + keys.length / 2]}!`);
  }
}

export default LoopCommand;
