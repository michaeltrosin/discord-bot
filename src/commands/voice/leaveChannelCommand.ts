import Command, { CommandCategory, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';
import { errors } from 'errors';

class LeaveChannelCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'leave';

  description = 'Mit diesem Befehl, verlässt der Bot den Channel, wenn du in dem Selben bist.';

  aliases: string[] = ['l', 'dc'];

  category: CommandCategory = CommandCategory.Voice;

  async execute(msg: Message<boolean>, parameters: Parameters): Promise<void> {
    const voiceChannel = msg.member?.voice.channel;

    if (!voiceChannel) {
      msg.reply(errors.voice.noVoiceChannel);
      return;
    }

    if (msg.guild === null) {
      return;
    }

    this.musicHandler.leave(voiceChannel);
    const queue = this.musicHandler.getQueue(msg.guild);
    // Clears the queue and skips the current playing song
    queue.clearQueue();
    queue.stopTrack();

    // Leave message
    msg.reply(`Verlasse den Channel ${voiceChannel.name}!`);
  }
}

export default LeaveChannelCommand;
