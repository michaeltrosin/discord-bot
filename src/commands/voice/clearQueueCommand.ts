import Command, { CommandCategory, Parameters } from '@/commands/command';
import MusicHandler from '@/music/musicHandler';
import { Message } from 'discord.js';

class ClearQueueCommand implements Command {
  constructor(private musicHandler: MusicHandler) { }

  name = 'clearQueue';

  description = 'Dieser Befehl löscht die Aktive Queue';

  aliases: string[] = ['cq'];

  category = CommandCategory.Voice;

  execute(msg: Message<boolean>, parameters: Parameters): void | Promise<void> {
    if (!msg.member?.voice.channel) {
      //TODO: No channel
      return;
    }

    if (!msg.guild) {
      return;
    }

    const guild = msg.guild;
    const queue = this.musicHandler.getQueue(guild);

    if (queue.length === 0) {
      msg.reply('Keine Songs in der Queue!');
      return;
    }

    queue.clearQueue();
    msg.reply('Die Queue wurde bereinigt!');
  }
}

export default ClearQueueCommand;
