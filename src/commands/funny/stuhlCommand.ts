import { ImageUtils } from '@/utils/avatarImage';
import { Message } from 'discord.js';
import Jimp from 'jimp';
import Command, { CommandCategory, CommandParameter } from '../command';

class StuhlCommand implements Command {
  name = 'stuhl';
  description = 'Stuhlt eine Person';
  aliases = [];
  parameters?: CommandParameter[] = [
    {
      name: 'user',
      description: 'Der nutzer, welcher gestuhlt werden soll. Muss @Nutzer sein.',
      required: true,
    },
  ];

  category = CommandCategory.Funny;

  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void> {
    if (parameters.length < 1) {
      msg.reply('Zu wenig parameter');
      return;
    }

    const user = msg.mentions.members?.first();
    if (user === null || user === undefined) {
      msg.reply('Zu wenige mentions');
      return;
    }

    Jimp.read('./assets/leosStuhl.png').then(async img => {
      if (img === null) {
        return;
      }

      const avatar = user?.avatarURL() ?? user?.user?.avatarURL();

      const file = await ImageUtils.avatarUrlToPng(avatar);
      const profile = await Jimp.read(file);

      const imgPosition = {
        x: img.getWidth() / 2 - 150,
        y: img.getHeight() / 2,
      };

      profile.scaleToFit(300, 300);

      img.blit(profile, imgPosition.x, imgPosition.y);

      const buffer = await img.getBufferAsync(Jimp.MIME_PNG);

      msg.channel.send({
        files: [
          {
            attachment: buffer,
            name: `gestuhlt.png`,
          },
        ],
      });
    });
  }
}

export default StuhlCommand;
