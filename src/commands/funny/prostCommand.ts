import { ImageUtils } from '@/utils/avatarImage';
import { Message } from 'discord.js';
import Jimp from 'jimp';
import Command, { CommandCategory, CommandParameter } from '../command';

class ProstCommand implements Command {
  name = 'prost';
  description = 'Stößt mit einer Person an';
  aliases = [];
  parameters?: CommandParameter[] = [
    {
      name: 'user',
      description: 'Der nutzer, mit wem angestoßen werden soll. Muss @Nutzer sein.',
      required: true,
    },
  ];

  category = CommandCategory.Funny;

  execute(msg: Message<boolean>, parameters: string[]): void | Promise<void> {
    if (parameters.length < 1) {
      msg.reply('Zu wenig parameter');
      return;
    }

    const user = msg.mentions.members?.first();
    if (user === null || user === undefined) {
      msg.reply('Zu wenige mentions');
      return;
    }

    Jimp.read('./assets/prost.png').then(async img => {
      if (img === null) {
        return;
      }

      const targetAvatar = user?.avatarURL() ?? user?.user?.avatarURL();
      const ownAvatar = msg.member?.avatarURL() ?? msg.member?.user?.avatarURL();

      const targetProfileImage = await Jimp.read(await ImageUtils.avatarUrlToPng(targetAvatar));
      const ownProfileImage = await Jimp.read(await ImageUtils.avatarUrlToPng(ownAvatar));

      const target = user?.nickname ?? user?.user.username;
      const own = msg.member?.nickname ?? msg.member?.user.username;

      const ownImagePosition = {
        x: img.getWidth() / 2 - 150,
        y: img.getHeight() / 2 - 60,
      };
      const targetImagePosition = {
        x: img.getWidth() / 2 + 40,
        y: img.getHeight() / 2 - 60,
      };

      const topText = {
        text: `${own} stößt mit ${target} an`,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_TOP,
      };

      const font = await Jimp.loadFont(Jimp.FONT_SANS_32_WHITE);

      img.print(font,
        0,
        0,
        topText,
        img.getWidth(),
        img.getHeight(),
      );

      targetProfileImage.scaleToFit(100, 100);
      ownProfileImage.scaleToFit(100, 100);
      ownProfileImage.flip(true, false);

      img.blit(targetProfileImage, targetImagePosition.x, targetImagePosition.y);
      img.blit(ownProfileImage, ownImagePosition.x, ownImagePosition.y);

      const buffer = await img.getBufferAsync(Jimp.MIME_PNG);

      msg.channel.send({
        files: [
          {
            attachment: buffer,
            name: `${own} stößt mit ${target}.png`,
          },
        ],
      });
    });
  }
}

export default ProstCommand;
